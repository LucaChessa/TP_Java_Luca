/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_java_luca;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.DocFlavor;
import jdk.nashorn.internal.objects.NativeString;

/**
 *
 * @author lchessa
 */
public class Math {

    private static final Logger LOG = Logger.getLogger(TP_Java_Luca.class.getName());

    public static void fact(int nb)
    {
        int nbfacto = 1;

        for(int i=1; i<=nb; i++)
        {            
            nbfacto = (i)*(nbfacto);            
        }

        System.out.println("Le resultat de la factorielle de "+ nb+ " est " + nbfacto);  

        Math.askFact();
        //On utilise une methode static car elle travail en interne sur un objet         

    }

        public static void askFact()
        {
            Scanner sc = new Scanner(System.in);
            int nb;
            String result = "";

            do
            {
            System.out.println("Veuillez saisir un nombre ou tapez Q pour quitter ");        

                if(sc.hasNextLine())
                {
                    result = sc.nextLine();

                    try
                    {
                    nb = Integer.parseInt(result);
                    Math.fact(nb);
                    }
                    catch(NumberFormatException e)
                    {                    
                    }   
            }
            }
            while(!"Q".equals(result.toUpperCase())); 
            }
     
        public static void pair(int[] tab)
        {
            ArrayList<String> tabPair = new  ArrayList<>();
            
            
            for(int i=0; i<tab.length; i++)
            {
                if(tab[i]%2==0)
                {                   
                   tabPair.add(String.valueOf(tab[i]));
                }
                
            }
           System.out.println("Les nombres pairs sont : "+ tabPair);      

        }
//        public static void fusion(int[] zboui, int[] zboui2)
//        {
//            
//          ArrayList<Integer> finalZboui = new  ArrayList<>();
//          int maxlength = zboui.length + zboui2.length ;
//          
//            
//           for(int i=0; i<maxlenght ; i++)
//            { 
//                if(finalZboui[i]%2==0)
//                {                   
//                   finalZboui.add(zboui[i]);
//                } 
//                else
//                {
//                    finalZboui.add(zboui2[i]);
//                }
//            }
           
            
//         System.out.println("Les nombres pairs sont : " );      
 
//        }
    
    public static void heure() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir un nombre de minutes : ");
        int result = 0;
        int heure = 0;
        int minute = 0;

        if (sc.hasNextInt()) {
            result = sc.nextInt();
            heure = result / 60;
            minute = result % 60;
        }

        System.out.println("La conversion en heure est : " + heure + ":" + minute);
    }

    public static void addition(int[] tab1, double[] tab2) {
        String calc = "";
        double result = 0.0;

        for (int entier : tab1) {
            calc += entier + " + ";
            result = result + entier;
        }

        int count = 0;
        for (double virgule : tab2) {
            if (count == tab2.length - 1) {
                calc += virgule + " = ";
            } else {
                calc += virgule + " + ";
            }
            result += virgule;
            count++;

        }
        calc += String.valueOf(result);
        System.out.println("Le résultat de l'addition est : " + calc);
    }

    public static void nombre() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir une valeur : ");
        int result = 0;
        int length = sc.nextInt();
        String calc = "";

        
        int count = 0;
        for (int i = 0; i <= length; i++) {
            
            if (count == length) {
                calc += i + " = ";
            } else {
                calc += i + " + ";
            }
          
            count++;
            result = result + i;
            

            
            if (result >= 100) {
                LOG.log(Level.SEVERE, "Le résultat dépasse 100 ");
                break;
            }
        }
        if(result < 100)
        System.out.println("Le résultat est : " +calc+result);
    }
    

    
    
}
